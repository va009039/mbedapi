// Package mbed Compile and Repository API client.
//
// References:
//   https://developer.mbed.org/handbook/Compile-API
//   https://developer.mbed.org/teams/mbed-api-users/wiki/Repository-API
package mbedapi

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"strings"
)

const (
	URL = "https://developer.mbed.org/api/v2/"
)

type Client struct {
	username, userpass string
	url                string
	repoMode,debug              bool
}

func New(username, userpass string) *Client {
	return &Client{username: username, userpass: userpass, url: URL}
}

func (c *Client) SetDebug(debug bool) {
	c.debug = debug
}

func (c *Client) SetUrl(url string) {
	c.url = url
}

type NewMessages struct {
	Severity string  `json:"severity"`
	Message  string  `json:"message"`
	Percent  float32 `json:"percent"`
	Action   string  `json:"action"`
	Type     string  `json:"type"`
	File     string  `json:"file"`
	Text     string  `json:"text"`
	Col      int     `json:"col"`
	Line     string  `json:"line"`
}

type LinkTotals struct {
	Code        int `json:"code"`
	Codeincdata int `json:"codeincdata"`
	ROData      int `json:"rodata"`
	RWData      int `json:"rwdata"`
	ZIData      int `json:"zidata"`
	Debug       int `json:"debug"`
}

type Data struct {
	Binary             string        `json:"binary"`
	TaskStatus         string        `json:"task_status"`
	Program            string        `json:"program"`
	TaskId             string        `json:"task_id"`
	FinisheddAt        string        `json:"finished_at"`
	TaskComplete       bool          `json:"task_complete"`
	CompilationSuccess bool          `json:"compilation_success"`
	NewMessages        []NewMessages `json:"new_messages"`
	StartedAt          string        `json:"started_at"`
	LinkTotals         LinkTotals    `json:"link_totals"`
	TimeTaken          float32       `json:"time_taken"`
}

type Result struct {
	Data Data `json:"data"`
}

type Response struct {
	Code   int    `json:"code"`
	Result Result `json:"result"`
}

func (c *Client) CompileStart(platform, prog string) (*Response, error) {
	values := url.Values{}
	values.Add("platform", platform)
        if strings.Index(prog, "https://") == 0 || strings.Index(prog, "http://") == 0 {
		values.Add("repo", prog)
                c.repoMode = true
	} else {
		values.Add("program", prog)
                c.repoMode = false
	}
	if c.debug {
		log.Println(values)
	}
	req, err := http.NewRequest("POST", c.url+"tasks/compiler/start/", strings.NewReader(values.Encode()))
	if err != nil {
		log.Fatal(err)
	}
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	resp, err := c.clientAuthDo(req)
	if err != nil {
		log.Fatal(err)
	}
	defer resp.Body.Close()
	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err)
	}
	if c.debug {
		log.Println(string(b))
	}
	var r Response
	err2 := json.Unmarshal(b, &r)
	if err2 != nil {
		log.Println(string(b))
		log.Println(r)
		log.Fatal(err2)
	}
	if r.Code != 200 {
		log.Fatal(string(b))
	}
	return &r, nil
}

func (c *Client) CompileStatus(taskId string) (*Response, error) {
	req, err := http.NewRequest("GET", c.url+"tasks/compiler/output/"+taskId, nil)
	if err != nil {
		log.Fatal(err)
	}
	resp, err := c.clientAuthDo(req)
	if err != nil {
		log.Fatal(err)
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		log.Fatal(resp)
	}
	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err)
	}
	if c.debug {
		log.Println(string(b))
	}
	var r Response
	err2 := json.Unmarshal(b, &r)
	if err2 != nil {
		log.Println(err2)
	}
	if r.Code != http.StatusOK {
		log.Fatal(r)
	}
	return &r, nil
}

func (c *Client) CompileDownload(lastRespo *Response, output string) {
	if output == "" {
		output = lastRespo.Result.Data.Binary
	}
	values := url.Values{}
	values.Add("binary", lastRespo.Result.Data.Binary)
	values.Add("task_id", lastRespo.Result.Data.TaskId)
	values.Add("program", lastRespo.Result.Data.Program)
	if c.repoMode {
		values.Add("repomode", "1")
	}
	req, err := http.NewRequest("GET", c.url+"tasks/compiler/bin/?"+values.Encode(), nil)
	if err != nil {
		log.Fatal(err)
	}
	resp, err := c.clientAuthDo(req)
	if err != nil {
		log.Fatal(err)
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		log.Fatal(resp)
	}
	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err)
	}
	// save binary file
	temp, err := ioutil.ReadFile(output)
	if err == nil && string(temp) == string(b) { // same file
		return
	}
	err2 := ioutil.WriteFile(output, b, 0666)
	if err2 != nil {
		log.Fatal(err2)
	}
	log.Println(output, len(b), "bytes")
}

func (c *Client) CompileCancel(taskId string) error {
	values := url.Values{}
	values.Add("task_id", taskId)
	//log.Println(values)
	req, err := http.NewRequest("POST", c.url+"tasks/compiler/cancel/", strings.NewReader(values.Encode()))
	if err != nil {
		log.Fatal(err)
	}
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	resp, err := c.clientAuthDo(req)
	if err != nil {
		log.Fatal(err)
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		log.Fatal(resp)
	}
	return nil
}

func (r *Response) TaskId() string {
        return r.Result.Data.TaskId
}

func (r *Response) TaskComplete() bool {
        return r.Result.Data.TaskComplete
}

func (r *Response) TaskStatus() string {
        return r.Result.Data.TaskStatus
}

func (r *Response) CompilationSuccess() bool {
        return r.Result.Data.CompilationSuccess
}

type RepoResponse struct {
	CreatedRepositoryUrl string `json:"created_repository_url"`
	Errors               Errors `json:"errors"`
	Error                bool   `json:"error"`
}

type Errors struct {
	Repotype []string `json:"repotype"`
	Name     []string `json:"name"`
}

func (c *Client) RepositoryCreate(name, description, visibility, repotype string) (*RepoResponse, error) {
	values := url.Values{}
	values.Add("name", name)
	values.Add("description", description)
	values.Add("visibility", visibility)
	values.Add("repotype", repotype) // prog or lib
	if c.debug {
		log.Println(values)
	}
	req, err := http.NewRequest("POST", c.url+"repo/", strings.NewReader(values.Encode()))
	if err != nil {
		log.Fatal(err)
	}
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	resp, err := c.clientAuthDo(req)
	if err != nil {
		log.Fatal(err)
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		log.Fatal(resp)
	}
	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err)
	}
	if c.debug {
		log.Println(string(b))
	}
	var r RepoResponse
	err2 := json.Unmarshal(b, &r)
	if c.debug {
		log.Println(r)
	}
	return &r, err2
}

func (c *Client) clientAuthDo(req *http.Request) (*http.Response, error) {
	req.SetBasicAuth(c.username, c.userpass)
	if c.debug {
		log.Println(req.URL)
		//log.Println(req.Header) // CAUTION: header have BASIC-auth.
	}
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return resp, err
	}
	if c.debug {
		log.Println(resp)
	}
	if resp.StatusCode != http.StatusOK {
		log.Fatal(resp)
	}
	return resp, err
}
